from abc import ABC, abstractproperty, abstractmethod

class Event(ABC):
    def __init__ (self, senders):
    self.senders = senders
        
    @abstractproperty
    def condition(self):
        pass
        
    @abstractproperty
    def message(self):
        pass
    @abstractmethod
    def check(self):
        if self.condition:
            for sender in self.senders:
                sender.send(self.message)
                
class MockEvent(Event):
    @property
    def condition(self):
        return True
    @property
    def check(self):
        return "Now you are millionaire"
    
